public class Vertex
{
	public double x;
	public double y;
	public double z;

	Vertex()
	{
		x = 0.0;
		y = 0.0;
		z = 0.0;
	}

	Vertex(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	Vertex(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Point2D projectPoint(ModelViewer modelViewer) //A simple orthographic projection
	{
		Point2D retval = new Point2D();
		retval.x = (int) ((modelViewer.getModelScale() * x));
		retval.y = (int) ((modelViewer.getModelScale() * y));

		return retval;
	}

	public double distance(Vertex v) //Distance from this vertex the passed vertex
	{
		return Math.sqrt(Math.pow(v.x - x, 2) + Math.pow(v.y - y, 2) + Math.pow(v.z - z, 2));
	}

	public void applyTransform(double[][] transformationMatrix)
	{
		double[][] vertexMatrix = new double[][] { { x, y, z, 1 } // For
																	// homogeneous
																	// coordinates
		};
		double[][] newVertexMatrix = Model.multiplyMatrix(vertexMatrix, transformationMatrix);
		x = newVertexMatrix[0][0];
		y = newVertexMatrix[0][1];
		z = newVertexMatrix[0][2];
	}
}