import java.awt.Color;

public class Triangle //All self-explanatory
{
	private Vertex v1;
	private Vertex v2;
	private Vertex v3;
	private Color colour;

	public Triangle(Vertex v1, Vertex v2, Vertex v3)
	{
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;
		colour = Color.GRAY;
	}

	public Triangle(Vertex v1, Vertex v2, Vertex v3, Color colour)
	{
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;
		this.colour = colour;
	}

	public Vertex getV1()
	{
		return v1;
	}

	public void setV1(Vertex v1)
	{
		this.v1 = v1;
	}

	public Vertex getV2()
	{
		return v2;
	}

	public void setV2(Vertex v2)
	{
		this.v2 = v2;
	}

	public Vertex getV3()
	{
		return v3;
	}

	public void setV3(Vertex v3)
	{
		this.v3 = v3;
	}

	public Color getColour()
	{
		return colour;
	}

	public void setColour(Color colour)
	{
		this.colour = colour;
	}
}