public class Point2D
{//All self-explanatory
	public int x;
	public int y;

	Point2D()
	{
		x = 0;
		y = 0;
	}

	Point2D(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
}