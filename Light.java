import java.awt.Color;

public class Light
{ //Could do with some work, an omni-directional light source
	public float intensity;
	public Color colour = Color.WHITE;

	public Vertex position;
	public Vector3D direction;
	public Light()
	{
		position = new Vertex(0, 0, 0);
		direction = new Vector3D(0, 0, 0);
		intensity = 10;
	}

	public Light(double x, double y, double z, float intensity, double vX, double vY, double vZ)
	{
		position = new Vertex(x, y, z);
		direction = new Vector3D(vX, vY, vZ);
		this.intensity = intensity;
	}
}
