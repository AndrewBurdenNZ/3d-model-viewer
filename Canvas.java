import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JPanel;

/**
 * The draw area.
 */
class Canvas extends JPanel
{
	private static final long serialVersionUID = 1L;
	ModelViewer modelViewer;
	private Model m_model;

	public Canvas(ModelViewer modelViewer)
	{
		setOpaque(true);
		this.modelViewer = modelViewer;
	}

	public void setModel(final Model model)
	{
		m_model = model;
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		final Graphics2D g2 = (Graphics2D) g.create();
		if (m_model == null)
			return;
		double[][] zbuffer = new double[getWidth()][getHeight()];
		ArrayList<Triangle> m_triangles = m_model.get_triangles();
		g2.setColor(Color.GRAY);
		g2.translate(getWidth() / 2, getHeight() / 2);
		g2.scale(1.0, -1.0); // flip the y-axis
		Point2D rayOrigin;
		double zDepth;
		int heightModifier = getHeight() / 2;
		int widthModifier = getWidth() / 2;
		int adjustedY;
		int adjustedX;

		for (double[] row : zbuffer)
		{
			Arrays.fill(row, -9999.0);
		}
		
		//I should probably put all this inside a triangle.draw() function, but it's more work than I have time for right now
		if (modelViewer.renderSolid())
		{
			for (int i = 0; i < m_triangles.size(); i++) //Loops through all the triangles
			{
				if (modelViewer.cullBackFaces()) //If the triangle is backfacing and cullBackFaces is on, skip that triangle
				{
					if (triangleIsBackfacing(m_triangles.get(i)))
						continue;
				}
				for (int y = minY(m_triangles.get(i)); y < maxY(m_triangles.get(i)); y++) //I think this is scanlines? Either way, for efficiency I only loop through the rectangle of pixels bounding the triangle
				{
					adjustedY = y + heightModifier; // Height modifier is for
													// centering the model in
													// the screen
					if ((adjustedY < 0) || (adjustedY >= getHeight())) //Not worried about pixels outside the screen
						continue;
					for (int x = minX(m_triangles.get(i)); x < maxX(m_triangles.get(i)); x++)
					{
						adjustedX = x + widthModifier;
						if ((adjustedX < 0) || (adjustedX >= getWidth()))
							continue;
						
						rayOrigin = new Point2D(x, y); //Ray casting for the z-buffering check

						if (pointWithinTriangle(m_triangles.get(i), rayOrigin)) //If the point is within the triangle...
						{
							zDepth = zValueOfPoint(m_triangles.get(i), rayOrigin); //We take its z value
							if (zDepth > zbuffer[adjustedX][adjustedY]) //And paint the pixel only if the z value is closer to the screen
							{
								zbuffer[adjustedX][adjustedY] = zDepth; //Then set the z buffer at that pixel to the new z value
								if (modelViewer.renderFlatShading())
								{
									g2.setColor(triangleColour(m_triangles.get(i))); //If there's shading, we have a particular colour
								} else
								{
									g2.setColor(Color.GRAY); //Otherwise just make it grey
								}
								drawDot(g2, rayOrigin); //If we were using some form of image buffer, I imagine this would be only a change in the z-buffer, which would get drawn later
							}
						}
					}
				}
			}
		}

		// Wireframe
		if (modelViewer.renderWireframe())
		{
			for (int i = 0; i < m_triangles.size(); i++)
			{
				if (modelViewer.cullBackFaces()) //Skip backfacing triangles
				{
					if (triangleIsBackfacing(m_triangles.get(i)))
						continue;
				}
				g2.setColor(Color.YELLOW); //Wireframe lines
				draw3DLine(g2, m_triangles.get(i).getV1(), m_triangles.get(i).getV2());
				draw3DLine(g2, m_triangles.get(i).getV1(), m_triangles.get(i).getV3());
				draw3DLine(g2, m_triangles.get(i).getV2(), m_triangles.get(i).getV3());
				g2.setColor(Color.RED); //Dots at each vertex
				drawBigDot(g2, m_triangles.get(i).getV1());
				drawBigDot(g2, m_triangles.get(i).getV2());
				drawBigDot(g2, m_triangles.get(i).getV3());
			}
		}

		// Axes
		g2.setColor(Color.green);
		draw3DLine(g2, new Vertex(0, 0, 0), new Vertex(1, 0, 0));

		g2.setColor(Color.red);
		draw3DLine(g2, new Vertex(0, 0, 0), new Vertex(0, 1, 0));

		g2.setColor(Color.blue);
		draw3DLine(g2, new Vertex(0, 0, 0), new Vertex(0, 0, 1));
	}

	private boolean triangleIsBackfacing(Triangle triangle) //Compares the surface normal of the triangle with the camera-to-triangle vector, to check backfacing
	{
		Vector3D surfaceNormal = getNormal(triangle);
		Vector3D cameraToTriangle = new Vector3D(0,0,1);
		if (cameraToTriangle.dotProduct(surfaceNormal) >= 0)
		{
			return true;
		}
		return false;
	}

	private Color triangleColour(Triangle triangle) // A very simplistic flat shading implementation I threw in for fun. Over the summer I may try some gouraud and phong
	{
		//It doesn't work very well >.>
		float lightIntensity = modelViewer.lightSource.intensity;
		Vector3D lightToTriangle = new Vector3D(modelViewer.lightSource.position, triangle.getV1());
		Vector3D surfaceNormal = getNormal(triangle);
		float colourAdjustment = (float) lightToTriangle.dotProduct(surfaceNormal);
		float r = Math.max(0, Math.min(lightIntensity * colourAdjustment, 1));
		float g = Math.max(0, Math.min(lightIntensity * colourAdjustment, 1));
		float b = Math.max(0, Math.min(lightIntensity * colourAdjustment, 1));
		return new Color(r, g, b);
	}

	private Vector3D getNormal(Triangle triangle) //Fairly self-explanatory, calculates the normal of the triangle based on vectors drawn among some of the points
	{
		Vector3D v1 = new Vector3D(triangle.getV1(), triangle.getV2());
		Vector3D v2 = new Vector3D(triangle.getV3(), triangle.getV2());
		return v1.crossProduct(v2);
	}

	//Self-explanatory
	public void draw3DLine(Graphics2D g2, Vertex start, Vertex finish)
	{
		Point2D s = start.projectPoint(modelViewer);
		Point2D f = finish.projectPoint(modelViewer);
		g2.drawLine(s.x, s.y, f.x, f.y);
	}

	//Self-explanatory
	public void drawBigDot(Graphics2D g2, Vertex position)
	{
		Point2D s = position.projectPoint(modelViewer);
		g2.drawLine(s.x, s.y, s.x + 1, s.y + 1);
		g2.drawLine(s.x + 1, s.y, s.x, s.y + 1);
	}

	//Self-explanatory
	public void drawBigDot(Graphics2D g2, Point2D p)
	{
		g2.drawLine(p.x, p.y, p.x + 1, p.y + 1);
		g2.drawLine(p.x + 1, p.y, p.x, p.y + 1);
	}

	//Self-explanatory
	public void drawDot(Graphics2D g2, Vertex position)
	{
		Point2D s = position.projectPoint(modelViewer);
		g2.drawLine(s.x, s.y, s.x, s.y);
	}

	//Self-explanatory
	public void drawDot(Graphics2D g2, Point2D p)
	{
		g2.drawLine(p.x, p.y, p.x, p.y);
	}

	//Would probably put this somewhere else if I had time to re-factor the code, used for calculating the bounds of the triangle for scanline(?)
	private int minX(Triangle triangle)
	{
		int minX = triangle.getV1().projectPoint(modelViewer).x;
		minX = Math.min(minX, triangle.getV2().projectPoint(modelViewer).x);
		minX = Math.min(minX, triangle.getV3().projectPoint(modelViewer).x);
		return minX;
	}

	private int maxX(Triangle triangle)
	{
		int maxX = triangle.getV1().projectPoint(modelViewer).x;
		maxX = Math.max(maxX, triangle.getV2().projectPoint(modelViewer).x);
		maxX = Math.max(maxX, triangle.getV3().projectPoint(modelViewer).x);
		return maxX;
	}

	private int minY(Triangle triangle)
	{
		int minY = triangle.getV1().projectPoint(modelViewer).y;
		minY = Math.min(minY, triangle.getV2().projectPoint(modelViewer).y);
		minY = Math.min(minY, triangle.getV3().projectPoint(modelViewer).y);
		return minY;
	}

	private int maxY(Triangle triangle)
	{
		int maxY = triangle.getV1().projectPoint(modelViewer).y;
		maxY = Math.max(maxY, triangle.getV2().projectPoint(modelViewer).y);
		maxY = Math.max(maxY, triangle.getV3().projectPoint(modelViewer).y);
		return maxY;
	}

	private double zValueOfPoint(Triangle triangle, Point2D rayOrigin) //Calculates the z value using the equation for a plane and solving for Z
	{
		Vector3D surfaceNormal = getNormal(triangle);
		double A = surfaceNormal.x;
		double B = surfaceNormal.y;
		double C = surfaceNormal.z;
		double dzdx = -A / C;
		double dzdy = -B / C;
		double rayZ = triangle.getV1().z * modelViewer.getModelScale() + dzdx
				* (rayOrigin.x - triangle.getV1().projectPoint(modelViewer).x) + dzdy
				* (rayOrigin.y - triangle.getV1().projectPoint(modelViewer).y);
		return rayZ;
	}

	private boolean pointWithinTriangle(Triangle triangle, Point2D rayOrigin) //Uses barycentric coordinates to check if the point is within the triangle (after projection)
	{
		Vector2D v0 = new Vector2D(triangle.getV3().projectPoint(modelViewer), triangle.getV1().projectPoint(
				modelViewer));
		Vector2D v1 = new Vector2D(triangle.getV2().projectPoint(modelViewer), triangle.getV1().projectPoint(
				modelViewer));
		Vector2D v2 = new Vector2D(rayOrigin, triangle.getV1().projectPoint(modelViewer));
		double d00 = v0.dotProduct(v0);
		double d01 = v0.dotProduct(v1);
		double d02 = v0.dotProduct(v2);
		double d11 = v1.dotProduct(v1);
		double d12 = v1.dotProduct(v2);

		double invDenom = 1.0 / (d00 * d11 - d01 * d01);
		double u = (d11 * d02 - d01 * d12) * invDenom;
		double v = (d00 * d12 - d01 * d02) * invDenom;

		// Check if point is in triangle
		if ((u >= 0) && (v >= 0) && ((u + v) <= 1))
		{
			return true;
		}
		return false;
	}
}