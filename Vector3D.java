public class Vector3D
{

	public double x;
	public double y;
	public double z;

	public Vector3D()
	{
		x = 0.0;
		y = 0.0;
		z = 0.0;
	}

	public Vector3D(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3D(Vertex v1, Vertex v2) // Constructs this vector from the
											// passed Vertices
	{
		this.x = v1.x - v2.x;
		this.y = v1.y - v2.y;
		this.z = v1.z - v2.z;
	}

	public Vector3D(Vector3D v1, Vertex v2) // Constructs this vector from the
											// passed Vertices
	{
		this.x = v1.x - v2.x;
		this.y = v1.y - v2.y;
		this.z = v1.z - v2.z;
	}

	public Vector3D crossProduct(Vector3D v) // Returns the cross product of the
												// passed vector and this vector
	{
		return new Vector3D(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}

	public double dotProduct(Vector3D v) // Returns the cross product of the
											// passed vector and this vector
	{
		return (x * v.x + y * v.y + z * v.z);
	}

	public double dotProduct(Vertex v) // Returns the cross product of the
										// passed vector and this vector
	{
		return (x * v.x + y * v.y + z * v.z);
	}
}
