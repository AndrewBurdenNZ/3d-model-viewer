import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

/**
 * This class can load model data from files and manage it.
 */
class Model
{
	private int m_numVertices;
	private int m_numTriangles;
	private Random rand = new Random();
	private float r = rand.nextFloat();
	private float green = rand.nextFloat();
	private float b = rand.nextFloat();

	// the largest absolute coordinate value of the untransformed model data
	private double m_maxSize;
	private double m_modelScale;

	private ArrayList<Vertex> m_vertices = new ArrayList<Vertex>();
	private ArrayList<Triangle> m_triangles = new ArrayList<Triangle>();

	private Model()
	{
	}

	/**
	 * Creates a {@link Model} instance for the data in the specified file.
	 * 
	 * @param file
	 *            The file to load.
	 * @return The {@link Model}, or null if an error occurred.
	 */
	public static Model loadModel(final File file, ModelViewer modelViewer)
	{
		final Model model = new Model();

		// read the data from the file
		if (!model.loadModelFromFile(file, modelViewer))
		{
			return null;
		}

		return model;
	}

	/**
	 * Reads model data from the specified file.
	 * 
	 * @param file
	 *            The file to load.
	 * @return True on success, false otherwise.
	 */
	protected boolean loadModelFromFile(final File file, ModelViewer modelViewer)
	{
		m_maxSize = 0.f;

		try (final Scanner scanner = new Scanner(file))
		{
			// the first line specifies the vertex count
			m_numVertices = scanner.nextInt();

			// read all vertex coordinates
			float x, y, z;
			for (int i = 0; i < m_numVertices; ++i)
			{
				// advance the position to the beginning of the next line
				scanner.nextLine();

				// read the vertex coordinates
				x = scanner.nextFloat();
				y = scanner.nextFloat();
				z = scanner.nextFloat();

				Vertex new_vertex = new Vertex(x, y, z);
				m_vertices.add(new_vertex);

				// determine the max value in any dimension in the model file
				m_maxSize = Math.max(m_maxSize, Math.abs(x));
				m_maxSize = Math.max(m_maxSize, Math.abs(y));
				m_maxSize = Math.max(m_maxSize, Math.abs(z));
			}
			// Scaling value to make the model fit nicely inside the viewport
			m_modelScale = ((0.30 * Math.min(modelViewer.getCanvasHeight(), modelViewer.getCanvasWidth())) / this
					.getMaxSize());
			modelViewer.setModelScale(m_modelScale);
			// the next line specifies the number of triangles
			scanner.nextLine();
			m_numTriangles = scanner.nextInt();

			// read all polygon data (assume triangles); these are indices into
			// the vertex array
			// indices but not including 0? O_O
			int v1, v2, v3;
			for (int i = 0; i < m_numTriangles; ++i)
			{
				scanner.nextLine();

				// the model files start with index 1, we start with 0
				v1 = scanner.nextInt() - 1;
				v2 = scanner.nextInt() - 1;
				v3 = scanner.nextInt() - 1;
				// Random colours made it easier to test backface culling and
				// z-buffering, but the colour value is not currently used
				r = rand.nextFloat();
				green = rand.nextFloat();
				b = rand.nextFloat();

				Triangle new_triangle = new Triangle(m_vertices.get(v1), m_vertices.get(v2), m_vertices.get(v3),
						new Color(r, green, b));

				m_triangles.add(new_triangle);

			}
		} catch (FileNotFoundException e)
		{
			System.err.println("No such file " + file.toString() + ": " + e.getMessage());
			return false;
		} catch (NoSuchElementException e)
		{
			System.err.println("Invalid file format: " + e.getMessage());
			return false;
		} catch (Exception e)
		{
			System.err.println("Something went wrong while reading the" + " model file: " + e.getMessage());
			e.printStackTrace();
			return false;
		}

		System.out.println("Number of vertices in model: " + m_numVertices);
		System.out.println("Number of triangles in model: " + m_numTriangles);

		return true;
	}

	/**
	 * Returns the largest absolute coordinate value of the original,
	 * untransformed model data.
	 */
	// Algorithm taken from http://stackoverflow.com/a/23817349/2538263, naive
	// implementation of O(n^3) which is fine for the size of matrices we have.
	// Strassen's algorithm is quicker but only for large matrices, which we
	// aren't concerned with.
	public static double[][] multiplyMatrix(double[][] m1, double[][] m2)
	{
		int m1Height = m1[0].length;
		int m2Width = m2.length;
		if (m1Height != m2Width)
			return null; // Height of the first matrix must be the same as the
							// width of the second
		int mRRowLength = m1.length;
		int mRColLength = m2[0].length;
		double[][] mResult = new double[mRRowLength][mRColLength];
		for (int i = 0; i < mRRowLength; i++)
		{ // rows from m1
			for (int j = 0; j < mRColLength; j++)
			{ // columns from m2
				for (int k = 0; k < m1Height; k++)
				{ // columns from m1
					mResult[i][j] += m1[i][k] * m2[k][j];
				}
			}
		}
		return mResult;
	}

	//Fairly self-explanatory, fills the matrix with 0s then places the rotations in the matrix and applies it to every vertex
	//If I were to do more work on this, I would perhaps implement rotation around an arbitrary axis
	public void rotate(double xAngle, double yAngle, double zAngle)
	{
		double[][] rotX;
		double[][] rotY;
		double[][] rotZ;
		double[][] rotationMatrix = new double[4][4];

		for (double[] row : rotationMatrix)
		{
			Arrays.fill(row, 0);
		}

		rotationMatrix[0][0] = rotationMatrix[1][1] = rotationMatrix[2][2] = rotationMatrix[3][3] = 1;
		if (xAngle != 0) //To cut down on unnecessary matrix multiplication, only multiply if there's any rotational change
		{
			rotX = new double[][] { { 1, 0, 0, 0 }, { 0, Math.cos(xAngle), -Math.sin(xAngle), 0 },
					{ 0, Math.sin(xAngle), Math.cos(xAngle), 0 }, { 0, 0, 0, 1 } // For
																					// homogeneous
																					// coordinates
			};
			rotationMatrix = multiplyMatrix(rotX, rotationMatrix);
		}

		if (yAngle != 0)
		{
			rotY = new double[][] { { Math.cos(yAngle), 0, Math.sin(yAngle), 0 }, { 0, 1, 0, 0 },
					{ -Math.sin(yAngle), 0, Math.cos(yAngle), 0 }, { 0, 0, 0, 1 } // For
																					// homogeneous
																					// coordinates
			};
			rotationMatrix = multiplyMatrix(rotY, rotationMatrix);
		}

		if (zAngle != 0)
		{
			rotZ = new double[][] { { Math.cos(zAngle), -Math.sin(zAngle), 0, 0 },
					{ Math.sin(zAngle), Math.cos(zAngle), 0, 0 }, { 0, 0, 1, 0 }, { 0, 0, 0, 1 } // For
																									// homogeneous
																									// coordinates
			};
			rotationMatrix = multiplyMatrix(rotZ, rotationMatrix);
		}

		for (int i = 0; i < m_vertices.size(); i++)
		{
			m_vertices.get(i).applyTransform(rotationMatrix);
		}
	}

	//Fairly self-explanatory, fills the matrix with 0s then places the scale values in the matrix and applies it to every vertex
	public void scale(double scaleValue)
	{
		double[][] scaleMatrix = new double[4][4];

		for (double[] row : scaleMatrix)
		{
			Arrays.fill(row, 0);
		}
		scaleMatrix[0][0] = scaleMatrix[1][1] = scaleMatrix[2][2] = scaleValue;

		for (int i = 0; i < m_vertices.size(); i++)
		{
			m_vertices.get(i).applyTransform(scaleMatrix);
		}
	}

	//Fairly self-explanatory, fills the matrix with 0s then places the translations in the matrix and applies it to every vertex
	public void translate(double translateX, double translateY, double translateZ)
	{
		double[][] translationMatrix = new double[4][4];

		for (double[] row : translationMatrix)
		{
			Arrays.fill(row, 0);
		}

		translationMatrix[3][0] = translateX;
		translationMatrix[3][1] = translateY;
		translationMatrix[3][2] = translateZ;
		translationMatrix[0][0] = translationMatrix[1][1] = translationMatrix[2][2] = translationMatrix[3][3] = 1;

		for (int i = 0; i < m_vertices.size(); i++)
		{
			m_vertices.get(i).applyTransform(translationMatrix);
		}
	}

	// Getters and setters
	public double getMaxSize()
	{
		return m_maxSize;
	}

	public double getModelScale()
	{
		return m_modelScale;
	}

	public void setM_modelScale(float m_modelScale)
	{
		this.m_modelScale = m_modelScale;
	}

	public ArrayList<Vertex> get_vertices()
	{
		return m_vertices;
	}

	public void set_vertices(ArrayList<Vertex> m_vertices)
	{
		this.m_vertices = m_vertices;
	}

	public ArrayList<Triangle> get_triangles()
	{
		return m_triangles;
	}

	public void set_triangles(ArrayList<Triangle> m_triangles)
	{
		this.m_triangles = m_triangles;
	}
}
