# README #

This model viewer opens the included .dat files and allows you to view the models included. There is also simplistic flat shading, as well as rotation, back-face culling, and so on.