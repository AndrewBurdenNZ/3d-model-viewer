public class Vector2D
{
	public int x;
	public int y;

	Vector2D()
	{
		x = 0;
		y = 0;
	}

	Vector2D(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	Vector2D(Point2D p1, Point2D p2)
	{
		this.x = p1.x - p2.x;
		this.y = p1.y - p2.y;
	}

	public double dotProduct(Point2D p) // Returns the cross product of the
										// passed vector and this vector
	{
		return (x * p.x + y * p.y);
	}

	public double dotProduct(Vector2D v) // Returns the cross product of the
											// passed vector and this vector
	{
		return (x * v.x + y * v.y);
	}
}